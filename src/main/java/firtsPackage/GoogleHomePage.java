package firtsPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;


public class GoogleHomePage {


    private static Logger LOG = LoggerFactory.getLogger(GoogleHomePage.class);
     WebDriver driver;

    @FindBy(xpath = "//textarea[@title='Search']")
    WebElement searchBar;

    @FindBy(xpath = "//input[@value='Google Search']")
    WebElement searchButton;

    @FindBy(xpath = "//input[@value=\"I'm Feeling Lucky\"]")
    WebElement luckyButton;

    @FindBy(xpath = "//img[@alt='Google']")
    WebElement googleImg;

    @FindBy(xpath = "//div[@id='result-stats']")
    WebElement resultStats;

    public GoogleHomePage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }
    public String validatePageLoad(){
        String title=driver.getTitle();
        System.out.println("Title ="+title);
        return title;
    }

    public String searchGoogle(String item) throws Exception {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.visibilityOf(searchBar));
        searchBar.sendKeys(item);
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();
        wait.until(ExpectedConditions.visibilityOf(resultStats));
        String results=resultStats.getText();
        System.out.println("Results ="+results);
        return results;
    }

    public String luckyGoogle(String item){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try{
            wait.until(ExpectedConditions.visibilityOf(searchBar));
        }
        catch(Exception e){
            googleImg.click();
            wait.until(ExpectedConditions.visibilityOf(searchBar));
        }

        searchBar.sendKeys(item);
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
        String title = driver.getTitle();
        System.out.println("Title ="+title);
        return title;
    }
}
