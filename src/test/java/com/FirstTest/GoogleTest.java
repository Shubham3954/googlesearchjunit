package com.FirstTest;

import firtsPackage.GoogleHomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;

public class GoogleTest {

    static WebDriver driver;

    GoogleHomePage googleHomePage =new GoogleHomePage(driver) ;


    @BeforeClass
    public static void openUrl(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.get("https://google.com");
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }

    @Test
    public void AvalidatePage(){
        googleHomePage.validatePageLoad();
    }
    @Test
    public void Bsearch() throws Exception {
        googleHomePage.searchGoogle("Hello");
    }

}
